<?php


namespace Hust\HotelBooking\Block\Product;


use Hust\HotelBooking\Helper\HotelProduct\RoomAction;
use Hust\HotelBooking\Helper\HotelProduct\RoomActionFactory;
use Magento\Catalog\Model\Product;
use Magento\Framework\View\Element\Template;

class RoomInfo extends Template
{
    protected $_product;

    protected $_registry;

    protected $roomActionFactory;

    public function __construct(
        Template\Context $context,
        \Magento\Framework\Registry $registry,
        RoomActionFactory $roomAction,
        array $data = []
    )
    {
        $this->_registry = $registry;
        $this->roomActionFactory = $roomAction;
        parent::__construct($context, $data);
    }

    /**
     * @return Product
     */
    public function getCurrentProduct()
    {
        if (!isset($this->_product)) {
            $this->_product = $this->_registry->registry('current_product');
        }

        return $this->_product;
    }

    public function isHotelProduct()
    {
        $product = $this->getCurrentProduct();

        return $product->getId() ? $product->getTypeId() == 'hotel_product' : false;
    }

    public function getRoomTypeDetails()
    {
        $product = $this->getCurrentProduct();
        $productId = $product->getId();

        $roomAction = $this->roomActionFactory->create();
        $roomType = $roomAction->getRoomTypeByProductId($productId);
        $roomTypeDetails = $roomType->getData();
        $roomTypeDetails['equips'] = $roomAction->getRoomEquip($roomType->getData('id'));

        return $roomTypeDetails;
    }

    public function getRoomProductDetails()
    {
        $product = $this->getCurrentProduct();
        $productId = $product->getId();

        $roomProductDetails = $this->roomActionFactory->create()->getRoomProductByProductId($productId)->getData();

        return $roomProductDetails;
    }

    public function getHotelDetails()
    {
        $product = $this->getCurrentProduct();
        $productId = $product->getId();
        $roomAction = $this->roomActionFactory->create();

        $roomType = $roomAction->getRoomTypeByProductId($productId)->getData();
        $hotelDetails = $roomAction->getHotelByRoomTypeId($roomType['id'])->getData();

        return $hotelDetails;
    }

    public function getFreeServiceDetails()
    {
        $product = $this->getCurrentProduct();
        $productId = $product->getId();
        $roomAction = $this->roomActionFactory->create();

        $roomProductId = $roomAction->getRoomProductByProductId($productId)->getData('entity_id');
        $freeServiceDetails = $roomAction->getFreeServices($roomProductId);
        return $freeServiceDetails;
    }
}