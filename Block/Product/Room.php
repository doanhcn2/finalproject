<?php


namespace Hust\HotelBooking\Block\Product;


use Magento\Framework\View\Element\Template;

class Room extends Template
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    protected $serviceCollectionFactory;

    /**
     * @var \Hust\HotelBooking\Helper\CatalogHelper
     */
    protected $_catalogHelper;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Checkout\Model\Cart $cart,
        \Hust\HotelBooking\Helper\CatalogHelper $_catalogHelper,
        \Hust\HotelBooking\Model\ResourceModel\Service\CollectionFactory $serviceCollectionFactory,
        Template\Context $context,
        array $data = []
    )
    {
        $this->coreRegistry = $registry;
        $this->_request = $context->getRequest();
        $this->_cart = $cart;
        $this->_catalogHelper = $_catalogHelper;
        $this->serviceCollectionFactory = $serviceCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function isRoomTypeProduct()
    {
        return $this->getCurrentProduct()->getTypeId() === 'hotel_product';
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getCurrentProduct()
    {
        return $this->coreRegistry->registry('current_product');
    }

    /**
     * @return int
     */
    public function getCurrentProductId()
    {
        return $this->getCurrentProduct()->getId();
    }

    public function getServices()
    {
        $servicesCollection = $this->serviceCollectionFactory->create();

        return $servicesCollection->getFirstItem()->getData();
    }

    /**
     * @return string
     */
    public function getCurrencySymbol()
    {
        return $this->_catalogHelper->getCurrentCurrencySymbol();
    }

    /**
     * @return array
     */
    public function checkEditCart()
    {
        $moduleName = $this->_request->getModuleName();
        $actionName = $this->_request->getActionName();
        $productId = $this->_request->getParam("product_id");

        $data = [];
        if ($moduleName == "checkout" && $actionName == "configure") {
            $items = $this->_cart->getQuote()->getItems();
            foreach ($items as $item) {
                /** @var \Magento\Quote\Model\Quote\Item $item */

                if ($item->getProductId() == $productId) {
                    $services = $item->getOptionByCode("services");
                    if ($services)
                        $services = $services->getData()['value'];
                    else
                        $services = [];
                    $checkInDate = $item->getOptionByCode("check_in_date")->getData();
                    $checkOutDate = $item->getOptionByCode("check_out_date")->getData();
                    $qty = $item->getOptionByCode("room_qty")->getData();
                    $data = [
                        "itemCartId" => $item->getId(),
                        'service' => $services ? json_decode($services, true) : [],
                        "check_in_date" => $checkInDate['value'],
                        "check_out_date" => $checkOutDate['value'],
                        'qty' => $qty['value']
                    ];
                }
            }
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        $product = $this->getCurrentProduct();
        $packageCol = $this->packageCollectionFactory->create()->addFieldToFilter('hotel_id', $product->getId());
        $minStart = null;
        foreach ($packageCol as $item) {
            if ($minStart == null || $item->getBookFrom() < $minStart) {
                $minStart = $item->getBookFrom();
            }
        }

        return $minStart ?: $this->getCurrentPackage()->getBookFrom();
    }
}