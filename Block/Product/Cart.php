<?php


namespace Hust\HotelBooking\Block\Product;


use Magento\Framework\View\Element\Template;

class Cart extends Template
{
    public function getText()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $quote = $objectManager->get(\Magento\Checkout\Model\Session::class)->getQuote();
        $items = $quote->getAllVisibleItems();
        $number = count($items);
        if ($number > 1)
            return $number . " items";

        return $number . " item";
    }
}