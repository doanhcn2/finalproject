<?php


namespace Hust\HotelBooking\Controller\Room;


use Hust\HotelBooking\Helper\Booking\CheckoutHelper;
use Hust\HotelBooking\Helper\HotelProduct\RoomAction;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class Book extends Action
{
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    protected $_cart;

    protected $_storeManager;

    protected $productRepository;

    protected $roomAction;

    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        RoomAction $roomAction,
        Context $context
    )
    {
        parent::__construct($context);
        $this->productRepository = $productRepository;
        $this->_storeManager = $storeManager;
        $this->_cart = $cart;
        $this->roomAction = $roomAction;
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
        try {
            $params = $this->getRequest()->getParams();
            $productId = @$params['productId'];
            $startDate = @$params['startDate'];
            $endDate = @$params['endDate'];
            $qty = @$params['qty'];
            $price = @$params['totalPrice'];

            $storeId = $this->_storeManager->getStore()->getId();
            $product = $this->productRepository->getById($productId, false, $storeId);

            $roomType = $this->roomAction->getRoomTypeByProductId($productId);
            $additionalOptions = [];
            $additionalOptions[] = [
                'label' => 'Room Type',
                'value' => $roomType->getData('name')
            ];
            $additionalOptions[] = [
                'label' => 'Check-in date',
                'value' => $startDate
            ];
            $additionalOptions[] = [
                'label' => 'Check-out date',
                'value' => $endDate
            ];
            $additionalOptions[] = [
                'label' => 'Qty',
                'value' => $qty
            ];

            $qtyPro = [
                'qty' => $qty,
            ];
            $request = new \Magento\Framework\DataObject($qtyPro);
            $quote = $this->_cart->getQuote();

            if (@$params['itemCartId']) {
                $item = $quote->getItemById($params['itemCartId']);
                $item->setProduct($product, $request);
            } else {
                $item = $quote->addProduct($product, $request);
            }

            $item->addOption([
                'product_id' => $productId,
                'code' => CheckoutHelper::HOTEL_ITEM_OPTION_ADDITIONAL,
                'value' => json_encode($additionalOptions)
            ]);
            $item->addOption([
                'product_id' => $productId,
                'code' => CheckoutHelper::HOTEL_ITEM_OPTION_ROOM_TYPE,
                'value' => $roomType->getData('id')
            ]);
            $item->addOption([
                'product_id' => $productId,
                'code' => CheckoutHelper::HOTEL_ITEM_OPTION_CHECK_IN_DATE,
                'value' => $startDate
            ]);
            $item->addOption([
                'product_id' => $productId,
                'code' => CheckoutHelper::HOTEL_ITEM_OPTION_CHECK_OUT_DATE,
                'value' => $endDate
            ]);
            $item->addOption([
                'product_id' => $productId,
                'code' => CheckoutHelper::HOTEL_ITEM_OPTION_ROOM_QTY,
                'value' => $qty
            ]);

            $item->setCustomPrice($price);
            $item->setOriginalCustomPrice($price);
            $item->getProduct()->setIsSuperMode(true);
            $this->_cart->save();

            $result->setData([
                'success' => true
            ]);

        } catch (\Exception $exception) {
            $result->setData([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }
        return $result;
    }
}