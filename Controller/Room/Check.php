<?php


namespace Hust\HotelBooking\Controller\Room;


use Hust\HotelBooking\Helper\HotelProduct\RoomAction;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class Check extends Action
{
    protected $roomAction;
    protected $_productFactory;
    public function __construct(
        RoomAction $roomAction,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        Context $context
    )
    {
        $this->_productFactory = $productFactory;
        $this->roomAction = $roomAction;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $result = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);

        try {
            $params = $this->getRequest()->getParams();
            $productId = @$params['productId'];
            $startDate = @$params['startDate'];
            $endDate = @$params['endDate'];
            $qty = @$params['qty'];
            $selectedRoomType = @$params['selectedRoomType'];

            $product = $this->_productFactory->create();

            $pricePerNight = $product->load($productId)->getPrice();
            $numNight = ((strtotime($endDate) - strtotime($startDate)) / 86400);

            // @todo: check is param valid here
            if (!is_int($qty) && $qty <= 0) {
                throw new \Exception(__("Qty is not valid"));
            }

//            $this->validateNightsNumber($startDate, $endDate, $package->getMinNights(), $package->getMaxNights());

            $roomType = $this->roomAction->getRoomTypeByProductId($productId);
            $roomAvailable = $this->roomAction->getRoomAvailableInTime($roomType->getData('id'), $startDate, $endDate);

            if ($qty > $roomAvailable) {
                throw new \Exception(__('You can book ' . $roomAvailable . ' room(s) in this time!'));
            }

            $result->setData([
                'success' => true,
                'data' => [],
                'total' => $pricePerNight * $numNight
            ]);

        } catch (\Exception $exception) {
            $result->setData([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }

        return $result;
    }

    /**
     * @param $start
     * @param $end
     * @param $min
     * @param $max
     * @throws \Exception
     */
    protected function validateNightsNumber($start, $end, $min, $max)
    {
        $num = ((strtotime($end) - strtotime($start)) / 86400) + 1;
        if ($num < 0) {
            throw new \Exception(__("End date is earlier than start date"));
        }
        if (!!$min && $num < $min) {
            throw new \Exception(__("Number of days must be greater or equal to %1", $min));
        }
        if (!!$max && $num > $max) {
            throw new \Exception(__("Number of days must be less or equal to %1", $max));
        }
    }
}