<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Room;


use Hust\HotelBooking\Controller\Adminhtml\Room;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Room
{
    protected $_roomTypeFactory;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        \Hust\HotelBooking\Model\RoomTypeFactory $roomTypeFactory
    )
    {
        $this->_roomTypeFactory = $roomTypeFactory;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_roomTypeFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Room no longer exists.'));
                $resultRedirect = $this->getResultPage();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('room_type', $model);

        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Hust_HotelBooking::equipments');
        $resultPage->getConfig()->getTitle()->prepend((__('Edit Equipment')));

        $resultPage->addBreadcrumb(
            $id ? __('Edit Room') : __('New Room'),
            $id ? __('Edit Room') : __('New Room')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Room'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? 'Edit Room ' : __('New Room'));

        return $resultPage;
    }
}