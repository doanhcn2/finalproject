<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Room;


use Hust\HotelBooking\Model\EquipmentsFactory;
use Hust\HotelBooking\Model\RoomEquipFactory;
use Hust\HotelBooking\Model\RoomFactory;
use Hust\HotelBooking\Model\RoomTypeFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;

class GetRoomDetails extends Action
{
    protected $_roomTypeFactory;

    protected $_roomFactory;

    protected $_equipmentsFactory;

    protected $_roomEquipFactory;

    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    public function __construct(
        RoomTypeFactory $roomTypeFactory,
        RoomFactory $roomFactory,
        EquipmentsFactory $equipmentsFactory,
        RoomEquipFactory $roomEquipFactory,
        Action\Context $context,
        JsonFactory $resultJsonFactory
    )
    {
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_roomTypeFactory = $roomTypeFactory;
        $this->_roomFactory = $roomFactory;
        $this->_equipmentsFactory = $equipmentsFactory;
        $this->_roomEquipFactory = $roomEquipFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        if ($this->getRequest()->isAjax())
        {
            $roomInfo = [];

            $room_id = $this->getRequest()->getParam('room_id');
            $roomTypeModel = $this->_roomTypeFactory->create();
            $roomTypeData = $roomTypeModel->load($room_id)->getData();
            $roomInfo['quality'] = $roomTypeData['quality'];
            $roomInfo['desc'] = $roomTypeData['description'];

            $roomModel = $this->_roomFactory->create();
            $roomCollection = $roomModel->getCollection()->addFieldToFilter('room_type_id', $room_id);
            $roomInfo['qty'] = count($roomCollection);

            $roomEquipModel = $this->_roomEquipFactory->create();
            $listRoomEquips = $roomEquipModel->getCollection()->addFieldToFilter('room_type_id', $room_id);
            foreach ( $listRoomEquips as $roomEquip) {
                $equipModel = $this->_equipmentsFactory->create();
                $equipData = $equipModel->load($roomEquip['room_equip_id']);
                $roomInfo['equips'][] = [
                    'name' => $equipData['name'],
                    'qty' => $roomEquip['qty']
                ];
            }
            return $result->setData($roomInfo);
        }
    }
}