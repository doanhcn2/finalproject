<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Room;


use Hust\HotelBooking\Controller\Adminhtml\Room;
use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\Type\FrontendPool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Delete extends Room
{
    protected $_roomTypeFactory;

    protected $_roomEquipFactory;

    protected $_roomFactory;

    /**
     * @var TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var FrontendPool
     */
    protected $_cacheFrontendPool;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        \Hust\HotelBooking\Model\RoomFactory $roomFactory,
        \Hust\HotelBooking\Model\RoomEquipFactory $roomEquipFactory,
        \Hust\HotelBooking\Model\RoomTypeFactory $roomTypeFactory,
        TypeListInterface $cacheTypeList,
        FrontendPool $cacheFrontendPool
    )
    {
        $this->_roomFactory = $roomFactory;
        $this->_roomEquipFactory = $roomEquipFactory;
        $this->_roomTypeFactory = $roomTypeFactory;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $roomTypeModel = $this->_roomTypeFactory->create();
                $roomTypeModel->load($id);

                $roomCollection = $this->_roomFactory->create()->getCollection()->addFieldToFilter('room_type_id', $id);
                foreach ($roomCollection as $room){
                    $roomModel = $this->_roomFactory->create();
                    $roomModel->load($room->getId());
                    $roomModel->delete();
                }

                $roomEquipCollection = $this->_roomEquipFactory->create()->getCollection()->addFieldToFilter('room_type_id', $id);
                foreach ($roomEquipCollection as $room){
                    $roomModel = $this->_roomEquipFactory->create();
                    $roomModel->load($room->getId());
                    $roomModel->delete();
                }
                $roomTypeModel->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the room.'));
                $this->_cacheTypeList->cleanType('config');
                foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                    $cacheFrontend->getBackend()->clean();
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a room to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}