<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Room;


use Hust\HotelBooking\Controller\Adminhtml\Room;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class imageUploader extends Room
{
    protected $_fileUploaderFactory;

    public function __construct(\Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory, Action\Context $context, Registry $coreRegistry, PageFactory $resultPageFactory)
    {
        $this->_fileUploaderFactory = $fileUploaderFactory;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $uploader = $this->_fileUploaderFactory->create(['fileId' => 'image']);

        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);

        $uploader->setAllowRenameFiles(false);

        $uploader->setFilesDispersion(false);

        $path = $this->_filesystem->getDirectoryRead(DirectoryList::MEDIA)

            ->getAbsolutePath('images/');

        $uploader->save($path);
    }
}