<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Room;


use Hust\HotelBooking\Controller\Adminhtml\Room;
use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\Type\FrontendPool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Save extends Room
{
    protected $_cacheTypeList;

    protected $_cacheFrontendPool;

    protected $_roomTypeFactory;

    protected $_roomFactory;

    protected $_roomEquipFactory;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        TypeListInterface $cacheTypeList,
        FrontendPool $cacheFrontendPool,
        \Hust\HotelBooking\Model\RoomTypeFactory $roomTypeFactory,
        \Hust\HotelBooking\Model\RoomFactory $roomFactory,
        \Hust\HotelBooking\Model\RoomEquipFactory $roomEquipFactory
    )
    {
        $this->_roomTypeFactory = $roomTypeFactory;
        $this->_roomFactory = $roomFactory;
        $this->_roomEquipFactory = $roomEquipFactory;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');
            $roomTypeModel = $this->_roomTypeFactory->create();
            if ($id) {
                $roomTypeModel->load($id);
            }

            $roomTypeData = array(
                'hotel_id' => $data['hotel_id'],
                'name' => $data['name'],
                'quality' => $data['quality'],
                'room_size' => $data['room_size'],
                'image' => json_encode(@$data['image']),
                'description' => @$data['description'],
            );
            $roomTypeModel->addData($roomTypeData);
            try {
                $roomTypeModel->save();

                // save list rooms
                $roomCollection = $this->_roomFactory->create()->getCollection()->addFieldToFilter('room_type_id', $roomTypeModel->getId());

                $list_room_id = [];
                if (isset($data['list_rooms_container'])){
                    foreach ($data['list_rooms_container'] as $room){
                        $roomModel = $this->_roomFactory->create();
                        if ($room['id']){
                            $roomModel->load($room['id']);
                        }

                        $roomModel->addData([
                            'room_type_id' => $roomTypeModel->getId(),
                            'room_number' => $room['room_number'],
                            'floor' => $room['floor'],
                            'description' => $room['description'],
                            'status' => $room['status'],
                        ]);
                        $roomModel->save();
                        $list_room_id[] = $roomModel->getId();
                    }
                }
                array_filter($list_room_id);
                foreach ($roomCollection as $room){
                    if (!in_array($room->getId(), $list_room_id)){
                        $roomModel = $this->_roomFactory->create();
                        $roomModel->load($room->getId());
                        $roomModel->delete();
                    }
                }



                // save list equipment
                $roomEquipCollection = $this->_roomEquipFactory->create()->getCollection()->addFieldToFilter('room_type_id', $roomTypeModel->getId());

                $list_equip_id = [];
                if (isset($data['list_equipments_container'])){
                    foreach ($data['list_equipments_container'] as $equip){
                        $roomEquipModel = $this->_roomEquipFactory->create();
                        if ($equip['id']){
                            $roomEquipModel->load($equip['id']);
                        }

                        $roomEquipModel->addData([
                            'room_type_id' => $roomTypeModel->getId(),
                            'equip_id' => $equip['room_equip_id'],
                            'qty' => $equip['qty'],
                        ]);
                        $roomEquipModel->save();
                        $list_equip_id[] = $roomEquipModel->getId();
                    }
                }
                array_filter($list_equip_id);
                foreach ($roomEquipCollection as $room){
                    if (!in_array($room->getId(), $list_equip_id)){
                        $roomModel = $this->_roomEquipFactory->create();
                        $roomModel->load($room->getId());
                        $roomModel->delete();
                    }
                }



                $this->messageManager->addSuccessMessage(__('Saved new room Successfully'));
                $this->_cacheTypeList->cleanType('config');
                foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                    $cacheFrontend->getBackend()->clean();
                }
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $roomTypeModel->getId()]);
                }
                return $resultRedirect->setPath('*/*/');

            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving Room.'));
            }
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}