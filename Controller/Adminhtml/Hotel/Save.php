<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Hotel;


use Hust\HotelBooking\Controller\Adminhtml\Hotel;
use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\Type\FrontendPool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Save extends Hotel
{
    protected $_cacheTypeList;

    protected $_cacheFrontendPool;

    protected $hotelManagementFactory;

    public function __construct(
        \Hust\HotelBooking\Model\HotelManagementFactory $hotelManagementFactory,
        Action\Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        TypeListInterface $cacheTypeList,
        FrontendPool $cacheFrontendPool
    )
    {
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->hotelManagementFactory = $hotelManagementFactory;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');
            $hotelManagementModel = $this->hotelManagementFactory->create();
            if ($id) {
                $hotelManagementModel->load($id);
            }

            $hotelData = array(
                'hotel_name' => $data['hotel_name'],
                'hotel_quality' => $data['hotel_quality'],
                'address' => $data['address'],
                'city' => $data['city'],
                'country' => $data['country'],
                'phone' => $data['phone'],
                'description' => @$data['description'],
            );
            $hotelManagementModel->addData($hotelData);
            try {
                $hotelManagementModel->save();

                $this->messageManager->addSuccessMessage(__('Saved new Hotel Successfully'));
                $this->_cacheTypeList->cleanType('config');
                foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                    $cacheFrontend->getBackend()->clean();
                }
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $hotelManagementModel->getId()]);
                }
                return $resultRedirect->setPath('*/*/');

            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving Hotel.'));
            }
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}