<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Hotel;


use Hust\HotelBooking\Controller\Adminhtml\Hotel;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class NewAction extends Hotel
{
    protected $forwardFactory;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $forwardFactory
    )
    {
        $this->forwardFactory = $forwardFactory;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $resultForward = $this->forwardFactory->create();
        return $resultForward->forward('edit');
    }
}