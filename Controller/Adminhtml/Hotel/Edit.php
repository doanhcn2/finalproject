<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Hotel;


use Hust\HotelBooking\Controller\Adminhtml\Hotel;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Hotel
{
    protected $hotelManagementFactory;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        \Hust\HotelBooking\Model\HotelManagementFactory $hotelManagementFactory,
        PageFactory $resultPageFactory
    )
    {
        $this->hotelManagementFactory = $hotelManagementFactory;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->hotelManagementFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Hotel no longer exists.'));
                $resultRedirect = $this->getResultPage();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('hotel_management', $model);

        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Hust_HotelBooking::hotel_management');
        $resultPage->getConfig()->getTitle()->prepend((__('Edit Hotel')));

        $resultPage->addBreadcrumb(
            $id ? __('Edit Hotel') : __('New Hotel'),
            $id ? __('Edit Hotel') : __('New Hotel')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Hotel'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? 'Edit Hotel ' : __('New Hotel'));

        return $resultPage;
    }
}