<?php


namespace Hust\HotelBooking\Controller\Adminhtml;


use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Magento\Ui\Component\MassAction\Filter;

abstract class Product extends Action
{
    /**
     * Core registry
     * @var Registry
     */
    protected $_coreRegistry;
    /**
     * Page factory
     * @var Page
     */
    protected $_resultPage;

    /**
     * Mass Action Filter
     * @var Filter
     */
    protected $_filter;

    /**
     * @var FileFactory
     */
    protected $_fileFactory;

    /**
     * @var PageFactory
     */
    protected  $_resultPageFactory;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        FileFactory $fileFactory,
        Filter $filter,
        PageFactory $resultPageFactory
    )
    {
        $this->_coreRegistry      = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        $this->_fileFactory       = $fileFactory;
        $this->_filter            = $filter;
        parent::__construct($context);
    }

    /**
     * instantiate result page object
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Framework\View\Result\Page
     */
    public function getResultPage()
    {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }

        return $this->_resultPage;
    }

    /**
     * set page data
     *
     * @return $this
     */
    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Hust_HotelBooking::products');
        $resultPage->getConfig()->getTitle()->prepend((__('Manage Hotel ')));
        return $this;
    }

    /**
     * Check ACL
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Hust_HotelBooking::products');
    }
}