<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Services;


use Hust\HotelBooking\Controller\Adminhtml\Services;
use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\Type\FrontendPool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Save extends Services
{
    protected $_serviceFactory;


    protected $_cacheTypeList;

    protected $_cacheFrontendPool;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        TypeListInterface $cacheTypeList,
        FrontendPool $cacheFrontendPool,
        \Hust\HotelBooking\Model\ServiceFactory $serviceFactory
    )
    {
        $this->_serviceFactory = $serviceFactory;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');
            $model = $this->_serviceFactory->create();
            if ($id) {
                $model->load($id);
            }

            $serviceData = array(
                'name' => $data['name'],
                'price' => $data['price'],
                'price_type' => $data['price_type'],
                'description' => $data['description'],
            );
            $model->addData($serviceData);
            try {
                $model->save();

                $this->messageManager->addSuccessMessage(__('Saved new service successfully'));
                $this->_cacheTypeList->cleanType('config');
                foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                    $cacheFrontend->getBackend()->clean();
                }
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');

            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving service.'));
            }
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}