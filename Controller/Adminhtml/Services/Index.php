<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Services;


use Hust\HotelBooking\Controller\Adminhtml\Services;
use Magento\Framework\App\ResponseInterface;

class Index extends Services
{

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $this->_setPageData();

        return $this->getResultPage();
    }
}