<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Services;


use Hust\HotelBooking\Controller\Adminhtml\Services;
use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\Type\FrontendPool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Delete extends Services
{
    protected $_serviceFactory;

    protected $_serviceAttrFactory;

    /**
     * @var TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var FrontendPool
     */
    protected $_cacheFrontendPool;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        \Hust\HotelBooking\Model\ServiceFactory $serviceFactory,
        \Hust\HotelBooking\Model\ServiceAttributesFactory $serviceAttributesFactory,
        TypeListInterface $cacheTypeList,
        FrontendPool $cacheFrontendPool
    )
    {
        $this->_serviceFactory = $serviceFactory;
        $this->_serviceAttrFactory = $serviceAttributesFactory;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $serviceModel = $this->_serviceFactory->create();
                $serviceModel->load($id);

                $serviceAttrCollection = $this->_serviceAttrFactory->create()->getCollection()->addFieldToFilter('service_id', $id);
                foreach ($serviceAttrCollection as $attr){
                    $serviceAttrModel = $this->_serviceAttrFactory->create();
                    $serviceAttrModel->load($attr->getId());
                    $serviceAttrModel->delete();
                }

                $serviceModel->delete();

                $this->messageManager->addSuccessMessage(__('You deleted the service.'));
                $this->_cacheTypeList->cleanType('config');
                foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                    $cacheFrontend->getBackend()->clean();
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a service to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}