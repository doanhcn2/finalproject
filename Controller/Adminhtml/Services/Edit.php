<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Services;


use Hust\HotelBooking\Controller\Adminhtml\Services;
use Hust\HotelBooking\Model\ServiceFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Services
{

    protected $_serviceFactory;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        ServiceFactory $serviceFactory,
        PageFactory $resultPageFactory
    )
    {
        $this->_serviceFactory = $serviceFactory;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }
    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $model = $this->_serviceFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Service no longer exists.'));
                $resultRedirect = $this->getResultPage();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('service', $model);

        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Hust_HotelBooking::services');
        $resultPage->getConfig()->getTitle()->prepend((__('Edit Service')));

        $resultPage->addBreadcrumb(
            $id ? __('Edit Service') : __('New Service'),
            $id ? __('Edit Service') : __('New Service')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Service'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? 'Edit Service ' : __('New Service'));

        return $resultPage;
    }
}