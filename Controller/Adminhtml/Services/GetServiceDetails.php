<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Services;


use Hust\HotelBooking\Model\ServiceAttributes;
use Hust\HotelBooking\Model\ServiceAttributesFactory;
use Hust\HotelBooking\Model\ServiceFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;

class GetServiceDetails extends Action
{
    protected $_serviceFactory;

    protected $_serviceAttributeFactory;

    /**
     * @var JsonFactory
     */
    protected $_resultJsonFactory;

    public function __construct(
        ServiceFactory $serviceFactory,
        ServiceAttributesFactory $serviceAttributesFactory,
        JsonFactory $resultJsonFactory,
        Action\Context $context
    )
    {
        $this->_serviceAttributeFactory = $serviceAttributesFactory;
        $this->_serviceFactory = $serviceFactory;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $result = $this->_resultJsonFactory->create();
        if ($this->getRequest()->isAjax()) {
            $serviceInfo = [];
            $serviceId = $this->getRequest()->getParam('service_id');

            $serviceModel = $this->_serviceFactory->create();
            $serviceAttributeModel = $this->_serviceAttributeFactory->create();

            $serviceData = $serviceModel->load($serviceId)->getData();
            $serviceAttributes = $serviceAttributeModel->getCollection()->addFieldToFilter('service_id', $serviceId);

            $serviceInfo['name'] = $serviceData['name'];
            $serviceInfo['desc'] = $serviceData['description'];

            foreach ($serviceAttributes as $attribute) {
                $attr = [
                    'name' => $attribute['service_key'],
                    'value' => $attribute['service_value']
                ];
                $serviceInfo['attr'][] = $attr;
            }
            $result->setData($serviceInfo);
        }

        return $result;
    }
}