<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Equipments;


use Hust\HotelBooking\Controller\Adminhtml\Equipment;
use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\Type\FrontendPool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Delete extends Equipment
{
    protected $equipAttrResourceFactory;

    protected $equipFactory;

    /**
     * @var TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * @var FrontendPool
     */
    protected $_cacheFrontendPool;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        \Hust\HotelBooking\Model\ResourceModel\EquipmentAttributesFactory $equipmentAttributesFactory,
        \Hust\HotelBooking\Model\EquipmentsFactory $equipmentFactory,
        TypeListInterface $cacheTypeList,
        FrontendPool $cacheFrontendPool
    )
    {
        $this->equipAttrResourceFactory = $equipmentAttributesFactory;
        $this->equipFactory = $equipmentFactory;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id');
        if ($id) {
            try {
                $model = $this->equipFactory->create();
                $model->load($id);
                $equipAttrResource = $this->equipAttrResourceFactory->create();
                $equipAttrResource->deleteAttr($id);
                $model->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the Equipment.'));
                $this->_cacheTypeList->cleanType('config');
                foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                    $cacheFrontend->getBackend()->clean();
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }
        $this->messageManager->addErrorMessage(__('We can\'t find a Equipment to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}