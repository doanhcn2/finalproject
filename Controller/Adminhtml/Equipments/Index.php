<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Equipments;


use Hust\HotelBooking\Controller\Adminhtml\Equipment;
use Magento\Framework\App\ResponseInterface;

class Index extends Equipment
{

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        $this->_setPageData();

        return $this->getResultPage();
    }
}