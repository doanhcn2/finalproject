<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Equipments;


use Hust\HotelBooking\Controller\Adminhtml\Equipment;
use Hust\HotelBooking\Model\EquipmentAttributesFactory;
use Hust\HotelBooking\Model\EquipmentsFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\Cache\Type\FrontendPool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Save extends Equipment
{
    protected $equipmentFactory;

    protected $equipmentAttrFactory;

    protected $equipAttrResourceFactory;

    protected $_cacheTypeList;

    protected $_cacheFrontendPool;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        TypeListInterface $cacheTypeList,
        FrontendPool $cacheFrontendPool,
        EquipmentsFactory $equipmentsFactory,
        EquipmentAttributesFactory $equipmentAttributesFactory,
        \Hust\HotelBooking\Model\ResourceModel\EquipmentAttributesFactory $equipAttrResourceFactory
    )
    {
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->equipmentFactory = $equipmentsFactory;
        $this->equipmentAttrFactory = $equipmentAttributesFactory;
        $this->equipAttrResourceFactory = $equipAttrResourceFactory;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('id');
            $model = $this->equipmentFactory->create();
            if ($id) {
                $model->load($id);
            }

            $equipmentData = array(
                'name' => $data['name'],
            );
            $model->addData($equipmentData);
            try {
                $model->save();

                // save attributes
                $equipAttrResource = $this->equipAttrResourceFactory->create();
                $equipAttrResource->deleteAttr($model->getId());
                foreach ($data['equipment_attributes_container'] as $attr){
                    $attrModel = $this->equipmentAttrFactory->create();
                    if ($attr['attribute_id']){
                        $attrModel->load($attr['attribute_id']);
                    }

                    $attrModel->addData([
                        'equipment_id' => $model->getId(),
                        'attr_key' => $attr['attr_key'],
                        'attr_value' => $attr['attr_value'],
                    ]);
                    $attrModel->save();
                }

                $this->messageManager->addSuccessMessage(__('Saved new equipment Successfully'));
                $this->_cacheTypeList->cleanType('config');
                foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                    $cacheFrontend->getBackend()->clean();
                }
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');

            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving equipment.'));
            }
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

}