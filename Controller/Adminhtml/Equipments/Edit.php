<?php


namespace Hust\HotelBooking\Controller\Adminhtml\Equipments;


use Hust\HotelBooking\Controller\Adminhtml\Equipment;
use Hust\HotelBooking\Model\EquipmentsFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends Equipment
{
    protected $_equipmentFactory;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        EquipmentsFactory $equipmentsFactory,
        PageFactory $resultPageFactory
    )
    {
        $this->_equipmentFactory = $equipmentsFactory;
        parent::__construct($context, $coreRegistry, $resultPageFactory);
    }

    /**
     * Execute action based on request and return result
     *
     * Note: Request will be added as operation argument in future
     *
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function execute()
    {

        $id = $this->getRequest()->getParam('id');
        $model = $this->_equipmentFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Equipment no longer exists.'));
                $resultRedirect = $this->getResultPage();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('equipment', $model);

        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Hust_HotelBooking::equipments');
        $resultPage->getConfig()->getTitle()->prepend((__('Edit Equipment')));

        $resultPage->addBreadcrumb(
            $id ? __('Edit Equipment') : __('New Equipment'),
            $id ? __('Edit Equipment') : __('New Equipment')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Equipment'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? 'Edit Equipment ' : __('New Equipment'));

        return $resultPage;
    }
}