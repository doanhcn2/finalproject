<?php


namespace Hust\HotelBooking\Controller\Adminhtml;


use Magento\Backend\App\Action;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

abstract class Hotel extends Action
{
    protected $_resultPage;

    protected $_resultPageFactory;

    protected $_coreRegistry;

    public function __construct(
        Action\Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory
    )
    {
        $this->_coreRegistry = $coreRegistry;
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function getResultPage() {
        if (is_null($this->_resultPage)) {
            $this->_resultPage = $this->_resultPageFactory->create();
        }

        return $this->_resultPage;
    }

    /**
     * set page data
     *
     * @return $this
     */
    protected function _setPageData()
    {
        $resultPage = $this->getResultPage();
        $resultPage->setActiveMenu('Hust_HotelBooking::hotel_management');
        $resultPage->getConfig()->getTitle()->prepend((__('Hotel Management')));

        return $this;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Hust_HotelBooking::hotel_management');
    }
}