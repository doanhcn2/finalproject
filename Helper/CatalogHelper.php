<?php


namespace Hust\HotelBooking\Helper;



use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class CatalogHelper extends AbstractHelper
{
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $_registry;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        Context $context
    )
    {
        $this->_storeManager = $storeManager;
        $this->_registry = $registry;
        parent::__construct($context);
    }

    public function getCurrentCurrencySymbol()
    {
        return $this->_storeManager->getStore()->getBaseCurrency()->getCurrencySymbol();
    }

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }

    /**
     * Get location string by: [location_name] - [city], [state], [country]
     * @param $productId
     * @return string
     */
    public function getLocationString($productId, $includeName = true)
    {
        return 'test';
    }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    public function getCurrentCurrencyCode()
    {
        return $this->_storeManager->getStore()->getBaseCurrency()->getCurrencyCode();
    }

    public function getCatalogMediaUrl()
    {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);

        return $mediaUrl . 'catalog/product';
    }
}