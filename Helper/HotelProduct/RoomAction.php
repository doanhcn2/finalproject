<?php


namespace Hust\HotelBooking\Helper\HotelProduct;


use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class RoomAction extends AbstractHelper
{
    protected $_productCollectionFactory;
    private $quoteItemFactory;
    private $itemResourceModel;

    protected $roomOrderItemCollectionFactory;
    protected $roomCollectionFactory;
    protected $hotelCollectionFactory;
    protected $roomTypeCollectionFactory;
    protected $roomProductCollectionFactory;
    protected $equipmentsCollectionFactory;
    protected $roomEquipCollectionFactory;
    protected $equipmentAttributesCollectionFactory;
    protected $serviceCollectionFactory;
    protected $serviceAttributeCollectionFactory;

    public function __construct(
        \Magento\Quote\Model\Quote\ItemFactory $quoteItemFactory,
        \Magento\Quote\Model\ResourceModel\Quote\Item $itemResourceModel,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\RoomOrderItem\CollectionFactory $roomOrderItemCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\Room\CollectionFactory $roomCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\RoomProduct\CollectionFactory $roomProductCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\HotelManagement\CollectionFactory $hotelCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\RoomType\CollectionFactory $roomTypeCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\RoomEquip\CollectionFactory $roomEquipCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\Equipments\CollectionFactory $equipmentsCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\EquipmentAttributes\CollectionFactory $equipmentAttributesCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\Service\CollectionFactory $serviceCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\ServiceAttributes\CollectionFactory $serviceAttributeCollectionFactory,
        Context $context
    )
    {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->roomCollectionFactory = $roomCollectionFactory;
        $this->roomOrderItemCollectionFactory = $roomOrderItemCollectionFactory;
        $this->roomProductCollectionFactory = $roomProductCollectionFactory;
        $this->hotelCollectionFactory = $hotelCollectionFactory;
        $this->roomTypeCollectionFactory = $roomTypeCollectionFactory;
        $this->roomEquipCollectionFactory = $roomEquipCollectionFactory;
        $this->equipmentsCollectionFactory = $equipmentsCollectionFactory;
        $this->equipmentAttributesCollectionFactory = $equipmentAttributesCollectionFactory;
        $this->serviceCollectionFactory = $serviceCollectionFactory;
        $this->serviceAttributeCollectionFactory = $serviceAttributeCollectionFactory;
        $this->quoteItemFactory = $quoteItemFactory;
        $this->itemResourceModel = $itemResourceModel;
        parent::__construct($context);
    }

    public function searchRoom($params)
    {
        $place = $params['place'];
        $bookingFrom = $params['booking_from'];
        $bookingTo = $params['booking_to'];
        $guest = $params['guest'];
        $room = $params['room'];

        /** @var \Hust\HotelBooking\Model\ResourceModel\HotelManagement\Collection $hotelCollection */
        $hotelCollection = $this->hotelCollectionFactory->create()->getHotelByLocation($place);
        $hotelIds = $hotelCollection->getAllIds();
        $productIds = [];
        foreach ($hotelIds as $hotelId) {
            /**
             * @var \Hust\HotelBooking\Model\ResourceModel\RoomType\Collection $roomTypeCollection
             */
            $roomTypeCollection = $this->roomTypeCollectionFactory->create()->getRoomTypeByHotelId($hotelId);

            $roomTypeIds = $roomTypeCollection->getAllIds();
            foreach ($roomTypeIds as $roomTypeId) {
                if ($this->getRoomAvailableInTime($roomTypeId, $bookingFrom, $bookingTo)) {
                    /**
                     * @var \Hust\HotelBooking\Model\ResourceModel\RoomProduct\Collection $roomProduct
                     */
                    $roomProduct = $this->roomProductCollectionFactory->create();
                    $productIds[] = $roomProduct->addFieldToFilter('room_type_id', $roomTypeId)->getFirstItem()->getData('product_id');
                }
            }
        }
        $productCollection = $this->_productCollectionFactory->create();
        $productCollection->addAttributeToFilter('entity_id', ['in' => $productIds]);

        return $productCollection;
    }

    public function getRoomAvailableInTime($roomTypeId, $bookingFrom, $bookingTo)
    {
        $roomOrdered = 0;

        $roomItemOrdered = $this->roomOrderItemCollectionFactory->create()->getRoomOrderedInTime($roomTypeId, $bookingFrom, $bookingTo);
        foreach ($roomItemOrdered as $item) {
            if ( (strtotime($bookingFrom) >= strtotime($item->getData('checkout')) )  || (strtotime($bookingTo) <= strtotime($item->getData('checkin'))) ){
                continue;
            }
            $quoteItem = $this->quoteItemFactory->create();
            $this->itemResourceModel->load($quoteItem, $item->getData('order_item_id'));
            $roomOrdered += $quoteItem->getQty();
        }

        $roomAvailable = $this->roomCollectionFactory->create()->getRoomAvailableByRoomType($roomTypeId);

        return (count($roomAvailable) - $roomOrdered);
    }

    public function getRoomTypeById($room_type_id)
    {
        $roomType = $this->roomTypeCollectionFactory->create()->addFieldToFilter('id', $room_type_id)->getFirstItem();
        return $roomType;
    }

    public function getRoomTypeByProductId($productId)
    {
        $roomProduct = $this->getRoomProductByProductId($productId);
        return $this->getRoomTypeById($roomProduct['room_type_id']);
    }

    public function getRoomProductByProductId($product_id)
    {
        $roomProduct = $this->roomProductCollectionFactory->create()->addFieldToFilter('product_id', $product_id)->getFirstItem();
        return $roomProduct;
    }

    public function getRoomProductById($roomProductId)
    {
        return $this->roomProductCollectionFactory->create()->addFieldToFilter('entity_id', $roomProductId)->getFirstItem();
    }

    /**
     * lay thong tin khach san
     * @param $room_type_id
     * @return \Magento\Framework\DataObject
     */
    public function getHotelByRoomTypeId($room_type_id)
    {
        $roomType = $this->getRoomTypeById($room_type_id);
        return $this->hotelCollectionFactory->create()->addFieldToFilter('id', $roomType['hotel_id'])->getFirstItem();
    }

    /**
     * lay thong tin thiet bi cua phong
     * @param $room_type_id
     */
    public function getRoomEquip($room_type_id)
    {
        $roomEquips = $this->roomEquipCollectionFactory->create()->addFieldToFilter('room_type_id', $room_type_id);
        $listEquips = [];
        foreach ($roomEquips as $roomEquip) {
            $roomEquipData = $roomEquip->getData();

            $equipment = $this->equipmentsCollectionFactory->create()->addFieldToFilter('id', $roomEquipData['equip_id'])->getFirstItem()->getData();
            $equipAttrs = $this->equipmentAttributesCollectionFactory->create()->addFieldToFilter('equipment_id', $equipment['id']);

            $equip['qty'] = $roomEquipData['qty'];
            $equip['name'] = $equipment['name'];

            foreach ($equipAttrs as $equipAttr) {
                $attr = $equipAttr->getData();
                $equip['attributes'][] = [
                    'attr_key' => $attr['attr_key'],
                    'attr_value' => $attr['attr_value']
                ];
            }

            $listEquips[] = $equip;
        }
    }

    /**
     * lay danh sach dich vu free di kem theo phong
     * @param $roomProductId
     * @return array
     */
    public function getFreeServices($roomProductId)
    {
        $roomProduct = $this->getRoomProductById($roomProductId)->getData();
        $serviceIds = json_decode($roomProduct['free_services'], true);
        $freeServices = [];
        foreach ($serviceIds as $id) {
            $freeServices[] = $this->getServiceById($id);
        }
        return $freeServices;
    }

    /**
     * lay thong tin dich vu noi chung
     * @param $serviceId
     * @return mixed
     */
    public function getServiceById($serviceId)
    {
        $service = $this->serviceCollectionFactory->create()->addFieldToFilter('id',$serviceId)->getFirstItem()->getData();
        $serviceAttrs = $this->serviceAttributeCollectionFactory->create()->addFieldToFilter('service_id', $serviceId);

        foreach ($serviceAttrs as $attr) {
            $attrData = $attr->getData();
            $service['attributes'][] = [
                'attr_key' => $attrData['service_key'],
                'attr_value' => $attrData['service_value']
            ];
        }
        return $service;
    }
}