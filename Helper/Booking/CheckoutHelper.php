<?php


namespace Hust\HotelBooking\Helper\Booking;


use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class CheckoutHelper extends AbstractHelper
{
    const HOTEL_ITEM_OPTION_SERVICES = 'services';
    const HOTEL_ITEM_OPTION_ROOM_TYPE = 'room_type';
    const HOTEL_ITEM_OPTION_ROOM_QTY = 'room_qty';
    const HOTEL_ITEM_OPTION_CHECK_IN_DATE = 'check_in_date';
    const HOTEL_ITEM_OPTION_CHECK_OUT_DATE = 'check_out_date';
    const HOTEL_ITEM_OPTION_ADDITIONAL = 'additional_options';

    protected $_orderFactory;

    protected $checkoutSession;

    protected $_orderRepository;

    public function __construct(
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        Context $context
    )
    {
        $this->_orderRepository = $orderRepository;
        $this->checkoutSession = $checkoutSession;
        $this->_orderFactory = $orderFactory;
        parent::__construct($context);
    }

    public function getOrderStatusByIncrementId($orderIncrementId)
    {
        /** @var \Magento\Sales\Model\Order $orderModel */
        $orderModel = $this->_orderFactory->create();
        $orderModel->loadByIncrementId($orderIncrementId);

        return $orderModel->getStatusLabel();
    }

    public function getSuccessInstructionPath($orderIncrementId)
    {
        /** @var \Magento\Sales\Model\Order $orderModel */
        $orderModel = $this->_orderFactory->create();
        $orderModel->loadByIncrementId($orderIncrementId);
        $paymentMethod = $orderModel->getPayment()->getMethod();
        $instructionPath = 'payment/' . $paymentMethod . '/instructions';

        return $this->getStoreConfig($instructionPath);
    }

    public function getStoreConfig($path, $store = null)
    {
        $value = $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE, $store);

        return $value;
    }

    public function isLastOrderHasShipping()
    {
        $orderId = $this->checkoutSession->getLastOrderId();
        try {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $this->_orderRepository->get($orderId);
            $hasShipping = $order->hasShippingAddressId();
        } catch (\Exception $e) {
            $hasShipping = true;
        }

        return $hasShipping;
    }
}