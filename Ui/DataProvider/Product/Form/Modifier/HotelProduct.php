<?php


namespace Hust\HotelBooking\Ui\DataProvider\Product\Form\Modifier;


use Magento\Catalog\Model\Locator\LocatorInterface;
use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Ui\Component\Form\Element\DataType\Text;
use Magento\Ui\Component\Form\Element\Select;
use Magento\Ui\Component\Form\Field;

class HotelProduct extends AbstractModifier
{
    const PRODUCT_TYPE = 'hotel_product';
    const CONTROLLER_ACTION_EDIT_PRODUCT = 'catalog_product_edit';
    const CONTROLLER_ACTION_NEW_PRODUCT = 'catalog_product_new';


    protected $arrayManager;

    /**
     * @var RequestInterface
     */
    protected $request;

    protected $meta = [];

    protected $context;

    /**
     * @var LocatorInterface
     */
    protected $locator;


    protected $serviceFactory;

    protected $roomTypeFactory;

    protected $freeServiceFactory;

    protected $_roomFactory;

    protected $roomEquip;

    public function __construct(
        \Hust\HotelBooking\Model\FreeServiceFactory $freeServiceFactory,
        \Hust\HotelBooking\Model\ServiceFactory $serviceFactory,
        \Hust\HotelBooking\Model\RoomEquipFactory $roomEquipFactory,
        \Hust\HotelBooking\Model\RoomTypeFactory $roomTypeFactory,
        \Hust\HotelBooking\Model\RoomFactory $roomFactory,
        \Magento\Backend\Block\Widget\Context $context,
        RequestInterface $request,
        LocatorInterface $locator,
        ArrayManager $arrayManager
    )
    {
        $this->roomEquip = $roomEquipFactory;
        $this->_roomFactory = $roomFactory;
        $this->roomTypeFactory = $roomTypeFactory;
        $this->serviceFactory = $serviceFactory;
        $this->freeServiceFactory = $freeServiceFactory;
        $this->context = $context;
        $this->request = $request;
        $this->locator = $locator;
        $this->arrayManager = $arrayManager;
    }

    /**
     * @param array $data
     * @return array
     * @since 100.1.0
     */
    public function modifyData(array $data)
    {
        $product = $this->locator->getProduct();
        $productId = $product->getId();
        if ($this->isHotelProduct()) {
            $roomType = $this->roomTypeFactory->create()->getCollection()->addFieldToFilter('product_id', $productId);
            if (!count($roomType)) {
                return $data;
            }

            $roomTyeData = $roomType->getFirstItem()->getData();

            $freeServices = $this->freeServiceFactory->create()->getCollection()->addFieldToFilter('room_type_id', $roomTyeData['id']);
            $listServiceIds = [];
            foreach ($freeServices as $freeService) {
                $listServiceIds[] = $freeService->getData('service_id');
            }

            $roomEquipCollection = $this->roomEquip->create()->getCollection()->addFieldToFilter('room_type_id', $roomTyeData['id']);
            $listRoomEquip = [];
            foreach ($roomEquipCollection as $roomEquip) {
                $listRoomEquip[] = [
                    'id' => $roomEquip->getData('id'),
                    'room_type_id' => $roomEquip->getData('room_type_id'),
                    'room_equip_id' => $roomEquip->getData('equip_id'),
                    'equipment_code' => $roomEquip->getData('code'),
                    'qty' => $roomEquip->getData('qty'),
                ];
            }

            $roomCollection = $this->_roomFactory->create()->getCollection()->addFieldToFilter('room_type_id', $roomTyeData['id']);
            $listRoom = [];
            foreach ($roomCollection as $room) {
                $listRoom[] = [
                    'id' => $room->getData('id'),
                    'room_type_id' => $room->getData('room_type_id'),
                    'room_number' => $room->getData('room_number'),
                    'floor' => $room->getData('floor'),
                    'description' => $room->getData('description'),
                    'status' => $room->getData('status'),
                ];
            }
            $data[strval($productId)]['hotel'] = [
                'room_type_info' => [
                    'room_type_id' => $roomTyeData['id'],
                    'room_size' => $roomTyeData['room_size'],
                    'checkin_from' => $roomTyeData['checkin_from'],
                    'checkin_to' => $roomTyeData['checkin_to'],
                    'checkout_from' => $roomTyeData['checkout_from'],
                    'checkout_to' => $roomTyeData['checkout_to'],
                    'quality' => $roomTyeData['quality'],
                    'hotel_id' => $roomTyeData['hotel_id'],
                    'free_services' => $listServiceIds
                ],
                'list_equipments_container' => $listRoomEquip,
                'list_rooms_container' => $listRoom
            ];
        }

        return $data;
    }

    /**
     * @param array $meta
     * @return array
     * @since 100.1.0
     */
    public function modifyMeta(array $meta)
    {
        $this->meta = $meta;
        if ($this->isHotelProduct()) {
            $this->getServiceForFree();
        }
        if ($this->isHotelProduct()) {
            unset($this->meta['product-details']['children']['quantity_and_stock_status_qty']);
            unset($this->meta['product-details']['children']['container_quantity_and_stock_status']);
            unset($this->meta['advanced-pricing']);
        } else {
            $this->meta['hotel']['arguments']['data']['config'] = [
                'disabled' => true,
                'visible' => false
            ];
//            $this->meta['hotel']['children']['hotel_product_properties']['children']['checkin_from']['arguments']['data']['config'] = [
//                'disabled' => true,
//            ];
//            $this->meta['hotel']['children']['hotel_product_properties']['children']['checkout_from']['arguments']['data']['config'] = [
//                'disabled' => true,
//            ];
        }
        return $this->meta;
    }

    private function listServices()
    {
        $serviceModel = $this->serviceFactory->create();
        $serviceCollection = $serviceModel->getCollection();

        $services[] = [
            'label' => __('Choose Service'),
            'value' => null
        ];

        foreach ($serviceCollection as $service) {
            $services[] = [
                'label' => $service->getName(),
                'value' => $service->getId()
            ];
        }

        return $services;
    }

    protected function getServiceForFree()
    {
        $this->meta = array_merge_recursive(
            $this->meta,
            [
                'hotel' => [
                    'children' => [
                        'room_information' => [
                            'children' => [
                                'free_services' => [
                                    'arguments' => [
                                        'data' => [
                                            'config' => [
                                                'options' => $this->listServices(),
                                            ],
                                        ],
                                    ],
                                ]
                            ]
                        ]

                    ]
                ]
            ]
        );
    }

    protected function urlGetRoomDetails()
    {
        return $this->getUrl('hotelsystem/room/getroomdetails');
    }

    protected function urlGetServiceDetails()
    {
        return $this->getUrl('hotelsystem/services/getservicedetails');
    }

    /**
     * @return bool
     */
    private function isHotelProduct()
    {
        $actionName = $this->request->getFullActionName();
        $isHotel = false;
        if ($actionName == self::CONTROLLER_ACTION_EDIT_PRODUCT) {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->locator->getProduct();
            if ($product->getTypeId() == self::PRODUCT_TYPE) {
                $isHotel = true;
            }
        } elseif ($actionName == self::CONTROLLER_ACTION_NEW_PRODUCT) {
            if (self::PRODUCT_TYPE == $this->request->getParam('type')) {
                $isHotel = true;
            }
        }

        return $isHotel;
    }

    /**
     * Generate url by route and parameters
     *
     * @param string $route
     * @param array $params
     * @return  string
     */
    private function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}