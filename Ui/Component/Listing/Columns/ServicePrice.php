<?php


namespace Hust\HotelBooking\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;

class ServicePrice extends Column
{

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['id'])) {
                    if ($item['price_type'] == 0) {
                        $item[$this->getData('name')] = '$' . $item[$this->getData('name')];
                    }
                    if ($item['price_type'] == 1) {
                        $item[$this->getData('name')] = '$' . $item[$this->getData('name')] .'/hour';
                    }
                    if ($item['price_type'] == 2) {
                        $item[$this->getData('name')] = '$' . $item[$this->getData('name')] . '/day';
                    }
                }
            }
        }
        return $dataSource;
    }
}