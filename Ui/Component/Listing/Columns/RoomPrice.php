<?php


namespace Hust\HotelBooking\Ui\Component\Listing\Columns;


use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

class RoomPrice extends Column
{
    protected $_productRepository;
    public function __construct(
        \Magento\Catalog\Model\ProductRepository $productRepository,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [], array $data = []
    )
    {
        $this->_productRepository = $productRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['id'])) {
                    $product = $this->_productRepository->getById($item['product_id']);
                    $item[$this->getData('name')] = '$' . $product->getPrice();
                }
            }
        }
        return $dataSource;
    }

}