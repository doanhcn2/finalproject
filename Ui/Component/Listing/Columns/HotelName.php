<?php


namespace Hust\HotelBooking\Ui\Component\Listing\Columns;


use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;

class HotelName extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $hotelManagementFactory;

    public function __construct(
        \Hust\HotelBooking\Model\HotelManagementFactory $hotelManagementFactory,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [], array $data = []
    )
    {
        $this->hotelManagementFactory = $hotelManagementFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if(isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $hotel_id = $item[$fieldName];
                $hotelModel = $this->hotelManagementFactory->create()->load($hotel_id);

                $item[$fieldName] = $hotelModel->getData('hotel_name');
            }
        }

        return $dataSource;
    }
}