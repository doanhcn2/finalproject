<?php


namespace Hust\HotelBooking\Ui\Component\Listing\Columns;


use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Store\Model\StoreManagerInterface;

class CountRoom extends \Magento\Ui\Component\Listing\Columns\Column
{
    protected $_roomFactory;

    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Hust\HotelBooking\Model\RoomFactory $roomFactory,
        array $components = [], array $data = [])
    {
        $this->_roomFactory = $roomFactory;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if(isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $room_type_id = $item['id'];
                $allRoom = $this->_roomFactory->create()->getCollection()->addFieldToFilter('room_type_id', $room_type_id);
                $roomReady = 0;
                foreach ($allRoom as $room) {
                    if ($room['status'] == 1){
                        $roomReady++;
                    }
                }
                $item[$fieldName] = $roomReady . ' room(s) ready/' . count($allRoom) .' room(s)';
            }
        }

        return $dataSource;
    }
}