<?php


namespace Hust\HotelBooking\Ui\Component\Room;


use Magento\Framework\Option\ArrayInterface;

class Quality implements ArrayInterface
{

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $options = [
            [
                'label' => '1 Sao',
                'value' => '1 Sao'
            ],
            [
                'label' => '2 Sao',
                'value' => '2 Sao'
            ],
            [
                'label' => '3 Sao',
                'value' => '3 Sao'
            ],
            [
                'label' => '4 Sao',
                'value' => '4 Sao'
            ],
            [
                'label' => '5 Sao',
                'value' => '5 Sao'
            ],
        ];

        return $options;
    }
}