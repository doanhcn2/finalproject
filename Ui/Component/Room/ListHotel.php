<?php


namespace Hust\HotelBooking\Ui\Component\Room;


use Magento\Framework\Option\ArrayInterface;

class ListHotel implements ArrayInterface
{
    protected $hotelManagementCollectionFactory;

    public function __construct(
        \Hust\HotelBooking\Model\ResourceModel\HotelManagement\CollectionFactory $hotelManagementCollectionFactory
    )
    {
        $this->hotelManagementCollectionFactory = $hotelManagementCollectionFactory;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $hotelManagementCollection = $this->hotelManagementCollectionFactory->create();

        $listHotel = [];
        foreach ($hotelManagementCollection as $hotel) {
            $listHotel[] = [
                'value' => $hotel->getData('id'),
                'label' => $hotel->getData('hotel_name')
            ];
        }

        return $listHotel;
    }
}