<?php


namespace Hust\HotelBooking\Ui\Component\Room\Equipment;


use Magento\Framework\Option\ArrayInterface;

class Options implements ArrayInterface
{
    protected $equipmentFactory;
    public function __construct(
        \Hust\HotelBooking\Model\EquipmentsFactory $equipmentsFactory
    )
    {
        $this->equipmentFactory = $equipmentsFactory;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $equipModel = $this->equipmentFactory->create();

        $equips = $equipModel->getCollection();
        $listEquips = [];
        foreach ($equips as $equip){
            $listEquips[] = [
                'label' => $equip->getName(),
                'value' => $equip->getId()
            ];
        }
        return $listEquips;
    }
}