<?php


namespace Hust\HotelBooking\Observer;


use Hust\HotelBooking\Helper\Booking\CheckoutHelper;
use Hust\HotelBooking\Model\RoomOrderItem;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order\Item;

class BookingOrderSave implements ObserverInterface
{
    protected $_quoteFactory;

    protected $_request;

    protected $isProcessed = false;

    protected $roomOrderItemFactory;

    public function __construct(
        \Magento\Quote\Model\QuoteFactory $quoteFactory,
        \Hust\HotelBooking\Model\RoomOrderItemFactory $roomOrderItemFactory,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->roomOrderItemFactory = $roomOrderItemFactory;
        $this->_request = $request;
        $this->_quoteFactory = $quoteFactory;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        if ($this->isProcessed) return;
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getData('order');
        $request = $this->_request->getParams();

        if (array_key_exists("creditmemo", $request)) {
            return;
        }

        $quoteId = $order->getQuoteId();
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->_quoteFactory->create()->load($quoteId);

        /** @var Item[] $items */
        $items = $order->getAllVisibleItems();
        foreach ($items as $salesItem) {
            if ($salesItem->getProductType() !== 'hotel_product' || $salesItem->getStatusId() === Item::STATUS_INVOICED) {
                continue;
            }

            $quoteItemId = $salesItem->getQuoteItemId();
            $item = $quote->getItemById($quoteItemId);
            $total = $salesItem->getRowTotalInclTax() ?: $salesItem->getRowTotal();

            $checkInDate = $item->getOptionByCode(CheckoutHelper::HOTEL_ITEM_OPTION_CHECK_IN_DATE)->getValue();
            $checkOutDate = $item->getOptionByCode(CheckoutHelper::HOTEL_ITEM_OPTION_CHECK_OUT_DATE)->getValue();
            $services = $item->getOptionByCode(CheckoutHelper::HOTEL_ITEM_OPTION_SERVICES);
            if ($services) {
                $services = $services->getValue();
            }
            $productId = $item->getProduct()->getId();
            $roomTypeId = $item->getOptionByCode(CheckoutHelper::HOTEL_ITEM_OPTION_ROOM_TYPE)->getValue();

            $roomOrderItemModel = $this->roomOrderItemFactory->create();

            $roomOrderData = [
                'order_item_id' => $item->getId(),
                'room_type_id' => $roomTypeId,
                'checkin' => $checkInDate,
                'checkout' => $checkOutDate,
                'note' => ''
            ];

            $roomOrderItemModel->addData($roomOrderData);
            $roomOrderItemModel->save();
        }
    }
}