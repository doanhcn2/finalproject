<?php


namespace Hust\HotelBooking\Observer\Product;


use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Salable implements ObserverInterface
{

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /**
         * @var $product Product
         */
        $product = $observer->getProduct();
        if ($product->getTypeId() == 'hotel_product') {
            $salable = $observer->getSalable();
            $salable->setIsSalable(true);
        }
    }
}