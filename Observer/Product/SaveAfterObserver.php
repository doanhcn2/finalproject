<?php


namespace Hust\HotelBooking\Observer\Product;


use Hust\HotelBooking\Model\ResourceModel\ServiceRoomProduct;
use Hust\HotelBooking\Model\RoomProduct;
use Hust\HotelBooking\Model\SalesPriceFactory;
use Hust\HotelBooking\Model\SalesPriceConditionFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Setup\Exception;
use Magento\Store\Model\StoreManagerInterface;

class SaveAfterObserver implements ObserverInterface
{
    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var ResourceConnection
     */
    protected $resource;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    protected $roomProductFactory;

    protected $_roomTypeFactory;

    protected $_roomFactory;

    protected $_roomEquipFactory;

    protected $freeServiceFactory;

    public function __construct(
        RequestInterface $request,
        StoreManagerInterface $storeManager,
        ResourceConnection $resource,
        Context $context,
        \Hust\HotelBooking\Model\RoomProductFactory $roomProductFactory,
        \Hust\HotelBooking\Model\RoomTypeFactory $roomTypeFactory,
        \Hust\HotelBooking\Model\RoomFactory $roomFactory,
        \Hust\HotelBooking\Model\RoomEquipFactory $roomEquipFactory,
        \Hust\HotelBooking\Model\FreeServiceFactory $freeServiceFactory,
        \Magento\Framework\Registry $registry
    )
    {
        $this->freeServiceFactory = $freeServiceFactory;
        $this->roomProductFactory = $roomProductFactory;
        $this->_roomTypeFactory = $roomTypeFactory;
        $this->_roomFactory = $roomFactory;
        $this->_roomEquipFactory = $roomEquipFactory;
        $this->_storeManager = $storeManager;
        $this->resource = $resource;
        $this->registry = $registry;
        $this->_request = $request;
        $this->messageManager = $context->getMessageManager();
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getEvent()->getProduct();
        if ($product->getTypeId() != 'hotel_product') {
            return;
        }
        $hotelDataRequest = $this->_request->getParam('hotel');

        // save room type
        $roomType = $hotelDataRequest['room_type_info'];
        $roomTypeModel = $this->_roomTypeFactory->create();
        if ($roomType['room_type_id']) {
            $roomTypeModel->load($roomType['room_type_id']);
        }
        $roomTypeData = array(
            'hotel_id' => @$roomType['hotel_id'],
            'name' => @$product->getName(),
            'quality' => @$roomType['quality'],
            'room_size' => @$roomType['room_size'],
            'image' => json_encode(@$roomType['image']),
            'checkin_from' => $roomType['checkin_from'],
            'checkin_to' => $roomType['checkin_to'],
            'checkout_from' => $roomType['checkout_from'],
            'checkout_to' => $roomType['checkout_to'],
            'product_id' => $product->getId(),
        );
        $roomTypeModel->addData($roomTypeData);
        $roomTypeModel->save();

        // save free service

        if (isset($roomType['free_services'])) {

            $freeServiceCollection = $this->freeServiceFactory->create()->getCollection()->addFieldToFilter('room_type_id', $roomTypeModel->getId());
            foreach ($freeServiceCollection as $freeService) {
                $freeServiceModel = $this->freeServiceFactory->create();
                $freeServiceModel->load($freeService->getId());
                $freeServiceModel->delete();
            }
            foreach ($roomType['free_services'] as $serviceId) {
                $roomEquipModel = $this->freeServiceFactory->create();

                $roomEquipModel->addData([
                    'room_type_id' => $roomTypeModel->getId(),
                    'service_id' => $serviceId,
                ]);
                $roomEquipModel->save();
            }
        }

        // save list rooms
        $roomCollection = $this->_roomFactory->create()->getCollection()->addFieldToFilter('room_type_id', $roomTypeModel->getId());

        $list_room_id = [];
        if (isset($hotelDataRequest['list_rooms_container'])) {
            foreach ($hotelDataRequest['list_rooms_container'] as $room) {
                $roomModel = $this->_roomFactory->create();
                if ($room['id']) {
                    $roomModel->load($room['id']);
                }

                $roomModel->addData([
                    'room_type_id' => $roomTypeModel->getId(),
                    'room_number' => @$room['room_number'],
                    'floor' => @$room['floor'],
                    'description' => @$room['description'],
                    'status' => @$room['status'],
                ]);
                $roomModel->save();
                $list_room_id[] = $roomModel->getId();
            }
        }
        array_filter($list_room_id);
        foreach ($roomCollection as $room) {
            if (!in_array($room->getId(), $list_room_id)) {
                $roomModel = $this->_roomFactory->create();
                $roomModel->load($room->getId());
                $roomModel->delete();
            }
        }

        // save list equipment
        $roomEquipCollection = $this->_roomEquipFactory->create()->getCollection()->addFieldToFilter('room_type_id', $roomTypeModel->getId());

        $list_equip_id = [];
        if (isset($hotelDataRequest['list_equipments_container'])) {
            foreach ($hotelDataRequest['list_equipments_container'] as $equip) {
                $roomEquipModel = $this->_roomEquipFactory->create();
                if ($equip['id']) {
                    $roomEquipModel->load($equip['id']);
                }

                $roomEquipModel->addData([
                    'room_type_id' => $roomTypeModel->getId(),
                    'equip_id' => @$equip['room_equip_id'],
                    'qty' => @$equip['qty'],
                    'code' => @$equip['equipment_code'],
                ]);
                $roomEquipModel->save();
                $list_equip_id[] = $roomEquipModel->getId();
            }
        }
        array_filter($list_equip_id);
        foreach ($roomEquipCollection as $room) {
            if (!in_array($room->getId(), $list_equip_id)) {
                $roomModel = $this->_roomEquipFactory->create();
                $roomModel->load($room->getId());
                $roomModel->delete();
            }
        }

        // save room product

//        $roomProductModel = $this->roomProductFactory->create();
//        $roomProduct = $hotelDataRequest['hotel_product_properties'];
//        $roomProductId = $hotelDataRequest['room_product_id'];
//
//        if ($roomProductId) {
//            $roomProductModel->load($roomProductId);
//        }
//
//        $hotelData = [
//            'checkin_from' => $roomProduct['checkin_from'],
//            'checkin_to' => $roomProduct['checkin_to'],
//            'checkout_from' => $roomProduct['checkout_from'],
//            'checkout_to' => $roomProduct['checkout_to'],
//            'free_services' => json_encode(isset($roomProduct['free_services']) ? $roomProduct['free_services'] : []),
//            'room_type_id' => $hotelDataRequest['room_type_info']['room_type_id'],
//            'product_id' => $product->getId(),
//        ];
//
//        $roomProductModel->addData($hotelData);
//        $roomProductModel->save();
    }

}