<?php


namespace Hust\HotelBooking\Observer\Backend\Product;


use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class StockUpdate implements ObserverInterface
{
    const DEFAULT_HOTEL_QTY = 999999;

    protected $_stockRegistryInterface;

    /**
     * StockUpdate constructor.
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     */
    public function __construct(
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
    ) {
        $this->_stockRegistryInterface = $stockRegistry;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        /**
         * @var $product Product
         */
        $product = $observer->getProduct();
        $stockItem = $this->_stockRegistryInterface->getStockItem($product->getId());
        $stockItem->setQty(self::DEFAULT_HOTEL_QTY);
        $stockItem->setIsInStock(true);
        try {
            $this->_stockRegistryInterface->updateStockItemBySku($product->getSku(), $stockItem);
        } catch (NoSuchEntityException $e) {
        }
    }
}