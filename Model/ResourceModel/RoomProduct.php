<?php


namespace Hust\HotelBooking\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class RoomProduct extends AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Room_Product', 'entity_id');
    }
}