<?php


namespace Hust\HotelBooking\Model\ResourceModel\ServiceRoomProduct;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Hust\HotelBooking\Model\ServiceRoomProduct', 'Hust\HotelBooking\Model\ResourceModel\ServiceRoomProduct');
    }
}