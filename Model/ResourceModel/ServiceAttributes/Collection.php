<?php


namespace Hust\HotelBooking\Model\ResourceModel\ServiceAttributes;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Hust\HotelBooking\Model\ServiceAttributes', 'Hust\HotelBooking\Model\ResourceModel\ServiceAttributes');
    }
}