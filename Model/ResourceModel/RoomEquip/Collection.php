<?php


namespace Hust\HotelBooking\Model\ResourceModel\RoomEquip;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    public function _construct()
    {
        $this->_init('Hust\HotelBooking\Model\RoomEquip', 'Hust\HotelBooking\Model\ResourceModel\RoomEquip');
    }
}