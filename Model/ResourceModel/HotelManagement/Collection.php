<?php


namespace Hust\HotelBooking\Model\ResourceModel\HotelManagement;



use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    public function _construct()
    {
        $this->_init('Hust\HotelBooking\Model\HotelManagement', 'Hust\HotelBooking\Model\ResourceModel\HotelManagement');
    }

    public function getHotelByLocation($location) {
        $this->getSelect()->where('address LIKE "%'. $location.'%" OR city LIKE "%' . $location .'%"');
        return $this;
    }
}