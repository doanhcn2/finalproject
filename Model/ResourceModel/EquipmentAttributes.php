<?php


namespace Hust\HotelBooking\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class EquipmentAttributes extends AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('equipment_attributes', 'id');
    }

    public function deleteAttr($equip_id)
    {
        $connection = $this->getConnection();
        $connection->delete(
            $this->getMainTable(),
            ['equipment_id = ?' => $equip_id]
        );

    }
}