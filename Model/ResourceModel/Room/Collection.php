<?php


namespace Hust\HotelBooking\Model\ResourceModel\Room;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Hust\HotelBooking\Model\Room', 'Hust\HotelBooking\Model\ResourceModel\Room');
    }

    public function getRoomAvailableByRoomType($roomTypeId) {
        $condition= 'room_type_id = ' . $roomTypeId . ' AND status = 1';
        $this->getSelect()->where($condition);
        return $this;
    }
}