<?php


namespace Hust\HotelBooking\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class FreeService extends AbstractDb
{

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Free_Service', 'id');
    }

}