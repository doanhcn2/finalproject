<?php


namespace Hust\HotelBooking\Model\ResourceModel\RoomProduct;



use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    protected function _construct()
    {
        $this->_init('Hust\HotelBooking\Model\RoomProduct', 'Hust\HotelBooking\Model\ResourceModel\RoomProduct');
    }
}