<?php


namespace Hust\HotelBooking\Model\ResourceModel\RoomType;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Hust\HotelBooking\Model\RoomType', 'Hust\HotelBooking\Model\ResourceModel\RoomType');
    }

    public function getRoomTypeByHotelId($hotel_id) {
        $this->getSelect()->where('hotel_id = ?', $hotel_id);
        return $this;
    }
}