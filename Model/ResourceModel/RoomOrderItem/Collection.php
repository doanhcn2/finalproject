<?php


namespace Hust\HotelBooking\Model\ResourceModel\RoomOrderItem;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Hust\HotelBooking\Model\RoomOrderItem', 'Hust\HotelBooking\Model\ResourceModel\RoomOrderItem');
    }

    /**
     * xem bao nhiêu phòng đã được đặt trong thời gian đó
     * @param $room_type_id
     * @param $startDate
     * @param $endDate
     */
    public function getRoomOrderedInTime ($room_type_id, $startDate, $endDate) {
        $query = 'room_type_id = ' . $room_type_id ;

        if (!empty($startDate) && !empty($endDate)) {
            $query .= ' AND ((checkin <= "' . $endDate . '" AND checkout >= "' . $endDate . '") OR (checkin <= "' . $startDate . '" AND checkout >=  "' . $startDate . '"))';
        } elseif (!empty($startDate) && empty($endDate)) {
            $query .= ' AND (checkin >= "' . $startDate . '" AND checkout < ' . $startDate . '")';
        } elseif (!empty($endDate) && empty($startDate)) {
            $query .= ' AND (checkin < "' . $endDate . '" AND checkout >= "' . $endDate . '")';
        }

        $this->getSelect()->where($query);
        return $this;
    }
}