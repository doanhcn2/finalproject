<?php


namespace Hust\HotelBooking\Model\ResourceModel\Service;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init('Hust\HotelBooking\Model\Service', 'Hust\HotelBooking\Model\ResourceModel\Service');
    }
}