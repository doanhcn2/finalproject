<?php


namespace Hust\HotelBooking\Model\Equipment;

use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    protected $equipAttributes;
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Hust\HotelBooking\Model\ResourceModel\Equipments\CollectionFactory $equipmentCollectionFactory,
        \Hust\HotelBooking\Model\EquipmentAttributesFactory $equipmentAttributesCollectionFactory,
        array $meta = [], array $data = [])
    {
        $this->collection = $equipmentCollectionFactory->create();
        $this->equipAttributes = $equipmentAttributesCollectionFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();

        $equipAttributes = $this->equipAttributes->create();

        $this->loadedData = array();
        foreach ($items as $template) {
            $this->loadedData[$template->getId()] = $template->getData();
            $this->loadedData[$template->getId()]['equipment_attributes_container'] = $equipAttributes->getCollection()->addFieldToFilter('equipment_id', $template->getId())->getData();
        }
        return $this->loadedData;
    }
}