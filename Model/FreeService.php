<?php


namespace Hust\HotelBooking\Model;


use Magento\Framework\Model\AbstractModel;

class FreeService extends AbstractModel
{
    public function _construct()
    {
        $this->_init('Hust\HotelBooking\Model\ResourceModel\FreeService');
    }
}