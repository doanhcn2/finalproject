<?php


namespace Hust\HotelBooking\Model;


use Magento\Framework\Model\AbstractModel;

class Equipments extends AbstractModel
{
    public function _construct()
    {
        $this->_init('\Hust\HotelBooking\Model\ResourceModel\Equipments');
    }

}