<?php


namespace Hust\HotelBooking\Model\Service;


use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    protected $serviceAttributes;
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Hust\HotelBooking\Model\ResourceModel\Service\CollectionFactory $serviceCollectionFactory,
        \Hust\HotelBooking\Model\ServiceAttributesFactory $serviceAttributesFactory,
        array $meta = [], array $data = [])
    {
        $this->collection = $serviceCollectionFactory->create();
        $this->serviceAttributes = $serviceAttributesFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();

        $serviceAttributes = $this->serviceAttributes->create();

        $this->loadedData = array();
        foreach ($items as $template) {
            $this->loadedData[$template->getId()] = $template->getData();
        }
        return $this->loadedData;
    }
}