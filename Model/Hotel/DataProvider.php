<?php


namespace Hust\HotelBooking\Model\Hotel;


use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    public function __construct(
        \Hust\HotelBooking\Model\ResourceModel\HotelManagement\CollectionFactory $collectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [], array $data = []
    )
    {
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();

        $this->loadedData = array();
        foreach ($items as $item) {
            $hotelData = [
                'id' => $item->getData()['id'],
                'hotel_name' => $item->getData()['hotel_name'],
                'hotel_quality' => $item->getData()['hotel_quality'],
                'address' => $item->getData()['address'],
                'city' => $item->getData()['city'],
                'country' => $item->getData()['country'],
                'phone' => $item->getData()['phone'],
                'description' => $item->getData()['description'],
            ];

            $this->loadedData[$item->getId()] = $hotelData;
        }
        return $this->loadedData;
    }
}