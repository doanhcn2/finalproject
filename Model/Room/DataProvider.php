<?php


namespace Hust\HotelBooking\Model\Room;

use Magento\Ui\DataProvider\AbstractDataProvider;

class DataProvider extends AbstractDataProvider
{
    protected $_roomFactory;

    protected $_roomEquipFactory;

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        \Hust\HotelBooking\Model\ResourceModel\RoomType\CollectionFactory $collectionFactory,
        \Hust\HotelBooking\Model\RoomFactory $roomFactory,
        \Hust\HotelBooking\Model\RoomEquipFactory $roomEquipFactory,
        array $meta = [],
        array $data = [])
    {
        $this->collection = $collectionFactory->create();
        $this->_roomFactory = $roomFactory;
        $this->_roomEquipFactory = $roomEquipFactory;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $roomModel = $this->_roomFactory->create();
        $roomEquipModel = $this->_roomEquipFactory->create();
        $items = $this->collection->getItems();

        $this->loadedData = array();
        foreach ($items as $roomTypeData) {
            $roomData = [
                'id' => $roomTypeData->getData()['id'],
                'name' => $roomTypeData->getData()['name'],
                'quality' => $roomTypeData->getData()['quality'],
                'room_size' => $roomTypeData->getData()['room_size'],
                'description' => $roomTypeData->getData()['description'],
                'list_rooms_container' => $roomModel->getCollection()->addFieldToFilter('room_type_id', $roomTypeData->getId())->getData(),
                'list_equipments_container' => $roomEquipModel->getCollection()->addFieldToFilter('room_type_id', $roomTypeData->getId())->getData()
            ];

            $this->loadedData[$roomTypeData->getId()] = $roomData;
        }
        return $this->loadedData;
    }
}