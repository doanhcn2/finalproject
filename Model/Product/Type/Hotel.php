<?php


namespace Hust\HotelBooking\Model\Product\Type;


class Hotel extends \Magento\Downloadable\Model\Product\Type
{
    const TYPE_ID = 'hotel_product';
    /**
     * Delete data specific for this product type
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return void
     */
    public function deleteTypeSpecificData(\Magento\Catalog\Model\Product $product)
    {

    }
}