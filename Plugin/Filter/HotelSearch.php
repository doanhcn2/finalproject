<?php


namespace Hust\HotelBooking\Plugin\Filter;

use Hust\HotelBooking\Helper\HotelProduct\RoomAction;
use Hust\HotelBooking\Model\ResourceModel\RoomProduct\Collection;
use Hust\HotelBooking\Model\RoomProduct;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Psr\Log\LoggerInterface;


class HotelSearch
{
    protected $_request;
    protected $hotelCollectionFactory;
    protected $roomTypeCollectionFactory;
    protected $roomProductCollectionFactory;
    protected $roomAction;

    public function __construct(
        RequestInterface $request,
        \Hust\HotelBooking\Helper\HotelProduct\RoomAction $roomAction,
        \Hust\HotelBooking\Model\ResourceModel\RoomProduct\CollectionFactory $roomProductCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\HotelManagement\CollectionFactory $hotelCollectionFactory,
        \Hust\HotelBooking\Model\ResourceModel\RoomType\CollectionFactory $roomTypeCollectionFactory
    )
    {
        $this->_request = $request;
        $this->roomAction = $roomAction;
        $this->roomProductCollectionFactory = $roomProductCollectionFactory;
        $this->hotelCollectionFactory = $hotelCollectionFactory;
        $this->roomTypeCollectionFactory = $roomTypeCollectionFactory;
    }
    /**
     * @param $subject
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $collection
     * @return mixed
     */
    public function afterGetHotelProductCollection($subject, $collection)
    {
        try {
            $place = $this->_request->getParam('place');
            $bookingFrom = $this->_request->getParam('booking_from');
            $bookingTo = $this->_request->getParam('booking_to');
            $guest = $this->_request->getParam('guest');
            $room = $this->_request->getParam('room');
            if ($place && !$collection->getFlag('filtered_by_place')) {
//                $collection->clear()->getSelect()->reset(\Zend_Db_Select::WHERE);
                /** @var \Hust\HotelBooking\Model\ResourceModel\HotelManagement\Collection $hotelCollection */
                $hotelCollection = $this->hotelCollectionFactory->create()->getHotelByLocation($place);
                $hotelIds = $hotelCollection->getAllIds();
                $productIds = [];
                foreach ($hotelIds as $hotelId) {
                    /**
                     * @var \Hust\HotelBooking\Model\ResourceModel\RoomType\Collection $roomTypeCollection
                     */
                    $roomTypeCollection = $this->roomTypeCollectionFactory->create()->getRoomTypeByHotelId($hotelId);

                    $roomTypeIds = $roomTypeCollection->getAllIds();
                    foreach ($roomTypeIds as $roomTypeId) {
                        if ($this->roomAction->getRoomAvailableInTime($roomTypeId, $bookingFrom, $bookingTo)) {
                            /**
                             * @var \Hust\HotelBooking\Model\ResourceModel\RoomProduct\Collection $roomProduct
                             */
                            $roomProduct = $this->roomProductCollectionFactory->create();
                            $productIds[] = $roomProduct->addFieldToFilter('room_type_id', $roomTypeIds)->getFirstItem()->getData('product_id');
                        }
                    }
                }
                $collection->addAttributeToFilter('entity_id', ['in' => $productIds]);
//                $collection->load();
                $collection->setFlag('filtered_by_place', true);

            }
//            $productName = $this->_request->getParam('guest');
//            if ($productName && !$collection->getFlag('filtered_by_guest')) {
//
//                $collection->addAttributeToFilter('name', ['like' => '%' . strtoupper($productName) . '%']);
//                $collection->setFlag('filtered_by_guest', true);
//            }
        } catch (\Exception $e) {
            ObjectManager::getInstance()->get(LoggerInterface::class)->critical('LocationFilterException: ' . $e->getMessage());
        }
        return $collection;
    }

    public function afterPrepareProductCollection($subject, $collection)
    {
        return $this;
    }

}