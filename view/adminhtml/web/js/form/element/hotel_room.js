define([
    'ko',
    'underscore',
    'jquery',
    'uiRegistry',
    'uiComponent',
    'Magento_Ui/js/form/element/select',
    'Magento_Ui/js/modal/modal',
], function (ko, _, $, registry, Componenet, select, modal) {
    'use strict';
    return select.extend({

        getRoomDetails: function(room_id) {
            var self = this;
            $.ajax({
                url: self.urlGetRoomDetails,
                type: 'POST',
                dataType: 'json',
                showLoader: true,
                data: {
                    room_id: room_id,
                },
                complete: function(response) {
                    var data = response.responseJSON;
                    self.hotelDetails(data);
                },
                error: function (xhr, status, errorThrown) {
                    console.log('Error happens. Try again.');
                }
            });
        },

        initialize: function (){
            this.hotelDetails = ko.observable();
            var room_id = this._super().initialValue;
            if (Number.isInteger(Number(room_id))){
                this.getRoomDetails(room_id);
            }
            return this;
        },

        /**
         * On value change handler.
         *
         * @param {String} value
         */
        onUpdate: function (value) {
            this.hotelDetails('');
            var room_id = Number(value);
            if (Number.isInteger(room_id)){
                this.getRoomDetails(room_id);
            }
            return this._super();
        }
    });
});